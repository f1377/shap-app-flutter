# Shop App [2019]

Build a Shop App [Followed Tutorial].

## Hint
To login into the Shop App, [**Shop App Server**](https://gitlab.com/majorvitec/shop-app-server) is **required**

## Login
- Username: **bibo**
- Password: **bib**

## Dependencies
- [provider](https://pub.dev/packages/provider)
- [intl](https://pub.dev/packages/intl)
- [http](https://pub.dev/packages/http)
- [shared_preferences](https://pub.dev/packages/shared_preferences)

## App Overview

### Login Screen

![Login Screen](/images/readme/login_screen.png "Login Screen")

### Products Overview Screen

![Products Overview Screen](/images/readme/overview_screen.png "Products Overview Screen")

### Drawer Screen

![Drawer Screen](/images/readme/drawer_screen.png "Drawer Screen")

### User Products Screen

![User Products Screen](/images/readme/products_screen.png "User Products Screen")

### Product Add / Edit Screen

![Product Add / Edit Screen](/images/readme/product_edit_screen.png "Product Add / Edit Screen")

### Orders Screen

![Orders Screen](/images/readme/orders_screen.png "Orders Screen")

### Product Details Screen

![Product Details Screen](/images/readme/product_details_screen.png "Product Details Screen")

### Filters Screen

![Filters Screen](/images/readme/filters_screen.png "Filters Screen")

### Cart Screen

![Cart Screen](/images/readme/cart_screen.png "Cart Screen")
