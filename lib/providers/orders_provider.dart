import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../models/order_item_model.dart';
import '../models/cart_item_model.dart';

import 'package:shop_app/helpers/server.dart';

class OrderProvider with ChangeNotifier {
  final String host = Server.host;
  List<OrderItemModel> _order = [];
  final String authToken;
  final String userId;

  OrderProvider(this.authToken, this.userId, this._order);

  List<OrderItemModel> get orders {
    // return copy
    return [..._order];
  }

  Map<String, String> createHeaders() {
    final String basicAuth =
        'Basic ' + base64Encode(utf8.encode('$authToken:'));

    return {
      HttpHeaders.authorizationHeader: basicAuth,
      HttpHeaders.contentTypeHeader: "application/json",
    };
  }

  Future<void> fetchAndSetOrders() async {
    final Uri url = Uri.parse(host + 'get_orders');
    final data = {
      "userId": userId.toString(),
    };

    try {
      final response = await http.post(
        url,
        body: json.encode(data),
        headers: createHeaders(),
      );
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      print(extractedData);
      final List<OrderItemModel> loadedOrder = [];

      if (extractedData == null) {
        return;
      }

      extractedData.forEach((orderId, orderData) {
        print(orderId);
        print(orderData);
        loadedOrder.add(OrderItemModel(
            id: orderId,
            amount: orderData['amount'],
            dateTime: DateTime.parse(orderData['dateTime']),
            products: (orderData['products'] as List<dynamic>)
                .map(
                  (item) => CartItemModel(
                    id: item['id'].toString(),
                    price: item['price'],
                    title: item['title'],
                    quantity: item['quantity'],
                  ),
                )
                .toList()));
      });

      _order = loadedOrder.reversed.toList();
      notifyListeners();
    } catch (error) {
      print(error);

      notifyListeners();
      throw error;
    }
  }

  Future<void> addOrder(List<CartItemModel> cartProducts, double total) async {
    final Uri url = Uri.parse(host + 'add_order');
    final timestamp = DateTime.now();
    final List<Map> cartItems = cartProducts
        .map(
          (cp) => {
            "id": cp.id,
            "title": cp.title,
            "quantity": cp.quantity,
            "price": cp.price,
          },
        )
        .toList();
    final Map data = {
      "userId": userId.toString(),
      "amount": total,
      "dateTime": timestamp.toIso8601String(),
      "products": cartItems,
    };

    try {
      final response = await http.post(
        url,
        body: json.encode(data),
        headers: createHeaders(),
      );

      // add at the beginnig of the list
      _order.insert(
        0,
        OrderItemModel(
          id: json.decode(response.body)['id'].toString(),
          amount: total,
          dateTime: timestamp,
          products: cartProducts,
        ),
      );
      notifyListeners();
    } catch (error) {
      print(error);
      notifyListeners();
      throw (error);
    }
  }
}
