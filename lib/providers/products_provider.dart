import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../models/http_exception.dart';
import './product_provider.dart';

import 'package:shop_app/helpers/server.dart';

class ProductsProvider with ChangeNotifier {
  final String host = Server.host;
  List<Product> _items = [];
  //   Product(
  //     id: 'p1',
  //     title: 'Red Shirt',
  //     description: 'A red shirt - it is pretty red!',
  //     price: 29.99,
  //     imageUrl:
  //         'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg',
  //   ),
  //   Product(
  //     id: 'p2',
  //     title: 'Trousers',
  //     description: 'A nice pair of trousers.',
  //     price: 59.99,
  //     imageUrl:
  //         'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Trousers%2C_dress_%28AM_1960.022-8%29.jpg/512px-Trousers%2C_dress_%28AM_1960.022-8%29.jpg',
  //   ),
  //   Product(
  //     id: 'p3',
  //     title: 'Yellow Scarf',
  //     description: 'Warm and cozy - exactly what you need for the winter.',
  //     price: 19.99,
  //     imageUrl:
  //         'https://live.staticflickr.com/4043/4438260868_cc79b3369d_z.jpg',
  //   ),
  //   Product(
  //     id: 'p4',
  //     title: 'A Pan',
  //     description: 'Prepare any meal you want.',
  //     price: 49.99,
  //     imageUrl:
  //         'https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Cast-Iron-Pan.jpg/1024px-Cast-Iron-Pan.jpg',
  //   ),
  // ];
  // var _showFavoritesOnly = false;
  final String authToken;
  final String userId;

  ProductsProvider(this.authToken, this.userId, this._items);

  List<Product> get items {
    // if (_showFavoritesOnly) {
    //   // return copy
    //   return _items.where((item) => item.isFavorite).toList();
    // }

    // return copy
    return [..._items];
  }

  List<Product> get favoriteItems {
    // return copy
    return _items.where((item) => item.isFavorite).toList();
  }

  Product findById(String id) {
    return _items.firstWhere((product) => product.id == id);
  }

  // void showFavoritesOnly() {
  //   _showFavoritesOnly = true;
  //   notifyListeners();
  // }

  // void showAll() {
  //   _showFavoritesOnly = false;
  //   notifyListeners();
  // }

  Map<String, String> createHeaders() {
    final String basicAuth =
        'Basic ' + base64Encode(utf8.encode('$authToken:something'));

    return {
      HttpHeaders.authorizationHeader: basicAuth,
      HttpHeaders.contentTypeHeader: "application/json",
    };
  }

  Future<void> fetchAndSetProduct([bool filterByUser = false]) async {
    final Uri url = Uri.parse(host + 'get_products');
    final data = {
      "userId": userId,
    };
    if (filterByUser) {
      data['filterByUser'] = filterByUser.toString();
    }

    try {
      final response = await http.post(
        url,
        body: json.encode(data),
        headers: createHeaders(),
      );
      print('response');
      print(json.decode(response.body));
      final List<Product> loadedProducts = [];
      final Map<String, dynamic>? extractedData =
          json.decode(response.body) as Map<String, dynamic>;

      if (extractedData == null) {
        return;
      }

      extractedData.forEach((productId, productData) {
        loadedProducts.add(Product(
          id: productId,
          title: productData['title'],
          description: productData['description'],
          price: productData['price'],
          imageUrl: productData['imageUrl'],
          isFavorite: productData['isFavorite'],
        ));
      });

      _items = loadedProducts;
      notifyListeners();
    } catch (error) {
      print(error);
      throw (error);
    }
  }

  // async: transforms the function to a Future and don't need a return anymore
  Future<void> addProduct(Product product) async {
    final Uri url = Uri.parse(host + 'add_product');
    final Map data = {
      "userId": userId.toString(),
      "title": product.title,
      "price": product.price.toString(),
      "description": product.description,
      "imageUrl": product.imageUrl,
      "isFavorite": product.isFavorite,
    };

    try {
      // await: doesn't go to the next line of code until response coming back
      final response = await http.post(
        url,
        body: json.encode(data),
        headers: createHeaders(),
      );
      print('response');
      print(json.decode(response.body));

      final newProduct = Product(
        id: json.decode(response.body)['id'].toString(),
        title: product.title,
        price: product.price,
        description: product.description,
        imageUrl: product.imageUrl,
      );

      _items.add(newProduct);
      // _items.insert(0, newProduct); // alternative add at the beginning
      notifyListeners();
    } catch (error) {
      print(error);
      // thor this error, so another widget can display it
      throw error;
    }
  }

  Future<void> updateProduct(String id, Product newProduct) async {
    final productIndex = _items.indexWhere((product) => product.id == id);

    if (productIndex >= 0) {
      final Uri url = Uri.parse(host + 'update_product');
      try {
        final Map data = {
          "id": newProduct.id,
          "title": newProduct.title,
          "price": newProduct.price.toString(),
          "description": newProduct.description,
          "imageUrl": newProduct.imageUrl,
        };

        print(data);
        await http.post(
          url,
          body: json.encode(data),
          headers: createHeaders(),
        );
        _items[productIndex] = newProduct;
      } catch (error) {
        print(error);
        throw (error);
      }

      notifyListeners();
    } else {
      print('No index has beend found!!!');
    }
  }

  Future<void> deleteProduct(String id) async {
    final Uri url = Uri.parse(host + 'delete_product');
    final existingProductIndex = _items.indexWhere((prod) => prod.id == id);
    Product existingProduct = _items[existingProductIndex];

    _items.removeAt(existingProductIndex);
    notifyListeners();

    final data = {
      "productId": id,
      "userId": userId,
    };
    final response = await http.post(
      url,
      body: json.encode(data),
      headers: createHeaders(),
    );
    if (response.statusCode >= 400) {
      _items.insert(existingProductIndex, existingProduct);
      notifyListeners();
      throw HttpExceptions('Could not delete product.');
    }
    // existingProduct = null;
  }
}
